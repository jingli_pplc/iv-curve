import matplotlib.pyplot as plt
import networkx as nx
import plot_figure.Draw_network as draw
import numpy as np
"""
Generate directed graph G: graph
name : Field 12
Nodes number :  15
"""

#
# class PV_grid:
#
#     def __init__(self, number_string, start_string, number_panel, parentStart_panel):
#         self.string_num = number_string # including string_start string
#         self.string_start =start_string
#         self.panel_num = number_panel
#         self.ID_parents_start = parentStart_panel
#         self.type = "Auto" # default
#
#     def generate_IDlist(self):
#
#         # Creating empty panel list
#         ID_list = []
#
#         # Creating empty string
#         string_PV = {}
#         #
#         # Creating empty ID parents
#         ID_parents = []
#
#         """
#         Given number of strings and panels
#         """
#         # Number of string in a array
#         string_end = self.string_start + self.string_num  # not cluding string_end string
#         string_list = np.arange(self.string_start - 1, string_end - 1)  # for ID_parents
#         print(string_list)
#         # Number of panels in a string
#         """
#         Node list
#         """
#         # Generate Node parents
#         ID_parents_start = self.ID_parents_start
#         # Chose the generate way:  Auto or manuel
#
#         if self.type == "manuel":
#             # ALL Panel ID connect to PCS
#             ID_parents.extend([ID_parents_start, 6, 11, 16, 21, 31])
#             # Auto ID parents
#         elif self.type == "Auto":
#             for string_ID in string_list:
#                 ID_parents.append(ID_parents_start + string_ID * self.panel_num)
#         print(ID_parents)
#         print(string_list)
#         # print("string : {}".format(string_list))
#
#         # Generate the ID list for each string
#         for string_name in string_list:
#             if string_name < string_end:
#                 # print(string_name)
#                 # print(ID_parents[string_name], panel_num)
#                 string_PV[str(string_name + 1)] = np.arange(ID_parents[string_name],
#                                                             ID_parents[string_name] + self.panel_num)
#                 ID_list.extend(string_PV[str(string_name + 1)])
#
#         # Check if there is a erro for the duplicate panel exited
#         ALL_lenth = len(ID_list)
#         set_lenth = len(set(ID_list))
#         if ALL_lenth != set_lenth:
#             print("There is a duplicate panel exited in different string,"
#                   " check the number of panels in each string and check the start panel ID of each string"
#                   "Or use the way of AUTO")
#
#         print(string_PV)
#         print(ID_list)
#         return ID_list


# Creating empty graph
G = nx.DiGraph()

# Creating empty panel list
ID_list = []

# Creating empty string
string_PV = {}

# Creating empty ID parents
ID_parents = []


"""
Given number of strings and panels
"""
# Number of string in a array
string_num = 5 # including string_start string
string_start = 1
string_end = string_start+string_num # not cluding string_end string
string_list = np.arange(string_start-1, string_end-1) # for ID_parents
print(string_list)
# Number of panels in a string
panel_num = 5

"""
Node list
"""
# Generate Node parents
ID_parents_start = 1

# Chose the generate way:  Auto or manuel
type = "Auto"

if type == "manuel":
    # ALL Panel ID connect to PCS
    ID_parents.extend([ID_parents_start, 6, 11,16,21,31])
    # Auto ID parents
elif type == "Auto":
    for string in string_list:
        ID_parents.append(ID_parents_start + string*panel_num)
print(ID_parents)
print(string_list)
# print("string : {}".format(string_list))

# Generate the ID list for each string
for string_name in string_list:
    if string_name < string_end:
        # print(string_name)
        # print(ID_parents[string_name], panel_num)
        string_PV[str(string_name+1)] = np.arange(ID_parents[string_name], ID_parents[string_name]+panel_num)
        ID_list.extend(string_PV[str(string_name + 1)])

# Check if there is a erro for the duplicate panel exited
ALL_lenth = len(ID_list)
set_lenth = len(set(ID_list))
if ALL_lenth != set_lenth:
    print("There is a duplicate panel exited in different string,"
          " check the number of panels in each string and check the start panel ID of each string"
          "Or use the way of AUTO")

print(string_PV)
print(ID_list)

# if __name__ == '__main__':
#
#     ins = PV_grid(4,2,5,1)
#
#     ID_list = ins.generate_IDlist()




node_list = ["parent"]
node_list.extend(ID_list)
print(node_list)
G.add_nodes_from(node_list)

# Add edges
# TODO modify the edge list here
edges_list = []
# node list who connect the parents node

for ID in ID_parents:
    edges_ins= [("parent",ID)]
    edges_list.extend(edges_ins)
print(edges_list)
G.add_edges_from(edges_list)

# # Select Graph layout
# pos = nx.layout.shell_layout(G)
#
# # Drawing nodes
# nodes = nx.draw_networkx_nodes(G, pos, node_size=20, node_color='Green')
# # Drawing edges
# edges = nx.draw_networkx_edges(G, pos, node_size=20, arrowstyle='->',
#                                        arrowsize=10, edge_cmp= plt.cm.Greys,
#                                        width=1.5)
#
# # draw network
# ins = draw.Draw_network(G, pos)
# ins.draw()




