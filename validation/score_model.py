from sklearn.metrics import r2_score, mean_squared_error, make_scorer


def mse(y_true,y_pred):
    mse = mean_squared_error(y_true, y_pred)
    print('MSE: %2.3f' % mse)
    return mse


def r2(y_true,y_pred):
     r2 = r2_score(y_true, y_pred)
     print('R2: %2.3f' % r2)
     return r2


def two_score(y_true,y_pred):
    mse(y_true,y_pred) #set score here and not below if using MSE in GridCV
    score = r2(y_true,y_pred)
    return score


def two_scorer():
    return make_scorer(two_score, greater_is_better=True) # change for false if using mse