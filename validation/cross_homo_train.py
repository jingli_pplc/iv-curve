from ivmodels import cell_irr

from sklearn.metrics import mean_squared_error
import numpy as np
import data.homo_subset as flux
from hyperopt import fmin, hp, anneal


"""
Given parameters
"""

# training steps
max_evals = 20000

# device related
cell_number = 60
cell_surface = 0.155**2-0.015**2*2

# hyper-parameter space
space = {
    'a': hp.uniform('a', 0.0, 1.0),
    'b': hp.uniform('b', 1E-8, 1E-3),
    "Is": hp.uniform('Is', 1.0E-19, 10.0E-17),
    "m": hp.uniform('m', 1.0, 1.5)
}

"""
Loading data from the all subset to radiation, temperature, voltage, current
"""

# All subset data
subls = flux.subls
list_len = flux.list_len
number_sub = flux.number_sub

"""
Calculate the loss based on the sub dataset
"""


def objective(parameters):
    a = parameters["a"]
    b = parameters["b"]
    Is = parameters["Is"]
    m = parameters["m"]
    # initialization parameters
    loss_list = []
    for i in np.arange(0, list_len):
    # loading data
        subls[i] = subls[i].sample(n=number_sub)
        radiation = subls[i].radiation
        cell_temperature = subls[i].Cell_temperature
        module_current = subls[i].current
        module_voltage = subls[i].voltage
        box_temperature = subls[i].Box_temperature
    # device related: Series collection of cells
        cell_irradiance = cell_surface*radiation
        cell_voltage = (module_voltage / cell_number)# voltage loss duel to cell series collection
        cell_current = module_current # current[id][t]
    # calculation erro
        current_estimator = cell_irr.IV_irr(cell_irradiance, cell_temperature, cell_voltage,
                                    a, b, Is, m)
        loss = mean_squared_error(cell_current, current_estimator)
        loss = loss**2
        loss_list.append(loss)
    # print(loss_list)
    erro_ave = 0
    for erro in loss_list:
        erro_ave = erro_ave + erro
        erro_ave = erro_ave / len(loss_list)
    # print(len(loss_list))
    # print(erro_ave)
    return erro_ave


"""
Optimization hyper parameters
"""
best = fmin(objective, space, algo=anneal.suggest, max_evals = max_evals)
print(best)

"""
plot the best parameters results
"""
best_m = best.get('m')
best_a = best.get('a')
best_b = best.get('b')
best_Is = best.get('Is')
print(best_a, best_b, best_Is, best_m)






