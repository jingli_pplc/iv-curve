import matplotlib.pyplot as plt
import networkx as nx
import plot_figure.Draw_network as draw
import numpy as np
import graphviz
"""
Generate directed graph G: graph
name : Field 12
Nodes number :  15
"""


class PV_grid:
    """
    reverse:
           if Ture -> panel of 305  in string 301~305 connect the parent node;
           if False -> panel of 301 connect the parent node
    """

    def __init__(self, number_string, start_string, number_panel, Start_panel, reverse):
        self.string_num = number_string # including string_start string
        self.string_start =start_string
        self.panel_num = number_panel
        self.ID_start = Start_panel
        self.type = "Auto" # default
        self.reverse = reverse

    def generate_IDlist(self):

        # Creating empty panel list
        ID_list = []

        # Creating empty string
        string_PV = {}
        #
        # Creating empty ID parents
        ID_parents = []

        """
        Given number of strings and panels
        """
        # Number of string in a array
        string_end = self.string_num  # not cluding string_end string
        string_list = np.arange(self.string_start - 1, string_end)  # for ID_parents
        print(string_list)
        # Number of panels in a string
        """
        Node list
        """
        # Generate Node parents
        ID_start = self.ID_start

        # Chose the generate way:  Auto or manuel
        if self.type == "manuel":
            # ALL Panel ID connect to PCS
            ID_parents.extend([ID_start, 6, 11, 16, 21, 31])
            # Auto ID parents
        elif self.type == "Auto":
            for string_ID in string_list:
                if self.reverse:
                    ID_parents.append(ID_start + (string_ID+1) * self.panel_num -1)
                else:
                    ID_parents.append(ID_start + string_ID * self.panel_num)

        # Generate the ID list for each string
        # Generate edge dictionary
        edges_dic = {}
        for string_item in np.arange(len(string_list)):
            if string_item < string_end:
                string_name = string_item + self.string_start
                if self.reverse:
                    string_PV[str(string_name)] = np.arange(ID_parents[string_item]+1 - self.panel_num,
                                                            ID_parents[string_item]+1)
                    for num in np.arange(self.panel_num):
                        start = ID_parents[string_item] +1 -num
                        end = ID_parents[string_item] - num
                        if end not in ID_parents:
                            edges_dic[str(end)] = [start, end]

                else:
                    string_PV[str(string_name)] = np.arange(ID_parents[string_item],
                                                            ID_parents[string_item] + self.panel_num)
                    for num in np.arange(self.panel_num):
                        start = ID_parents[string_item] +num
                        end = ID_parents[string_item] + num +1
                        if end not in ID_parents and end < (ID_start + string_end * self.panel_num):
                            edges_dic[str(end)] = [start, end]
                # string_PV[str(string_name)] = np.arange(ID_parents[string_item],
                #                                         ID_parents[string_item] + self.panel_num)
                ID_list.extend(string_PV[str(string_name)])
        # print(edges_dic)
        # Check if there is a erro for the duplicate panel exited
        ALL_lenth = len(ID_list)
        set_lenth = len(set(ID_list))
        if ALL_lenth != set_lenth:
            print("There is a duplicate panel exited in different string,"
                  " check the number of panels in each string and check the start panel ID of each string"
                  "Or use the way of AUTO")

        return {"ID_list":ID_list, "PV_string": string_PV, "ID_parents": ID_parents, "edges_dic": edges_dic}


    def generate_node(self, ID_list):
        # Creating empty node list
        node_list = []
        # Creating parents node
        # node_parents = parents_list
        # node_list.extend(node_parents)
        node_list.extend(ID_list)
        return node_list

    def generate_edge(self, node_parents, ID_parents, edges):
        # Generate edge list
        edges_list = []
        # node list who connect the parents node
        for ID in ID_parents:
            edges_parent= [(node_parents,ID)]
            edges_list.extend(edges_parent)
        # add edges list from edge_dic
        edges_list.extend(edges)
        return edges_list




if __name__ == '__main__':
    # Generate PV system size
    string_number = 3
    start_string = 1
    panel_number = 5
    start_panel = 301
    reverse = False
    ins_network = PV_grid(string_number,start_string,panel_number,start_panel, reverse)
    # Generate panel list
    id_ins = ins_network.generate_IDlist()
    ID_list = id_ins["ID_list"]
    ID_parents = id_ins["ID_parents"]
    PV_string = id_ins["PV_string"]
    edge_dic = id_ins["edges_dic"]
    print(ID_list)
    print(ID_parents)
    print(PV_string["2"])
    print(edge_dic)

    # Generate node list including PCS: parent node
    node_parents = "parent"
    node_list = ins_network.generate_node(ID_list)
    print(node_list)

    # Generate edge list
    edges_list = ins_network.generate_edge(node_parents, ID_parents,edge_dic.values())
    print(edges_list)

    # Generate Graph and name it
    G = nx.DiGraph()
    G.graph["name"] = "filed12"
    print(G.graph)

    # Read data from database
    voltage = {}
    current = {}
    irrandiance = {}
    temp_cell = {}

    # Generate node
    # G.add_nodes_from(node_list)
    for ID in node_list:
        voltage[ID] = 32
        current[ID] = 8
        irrandiance[ID] = 800
        temp_cell[ID] = 300
        G.add_node(ID, voltage = voltage[ID],
                       current = current[ID], irrandiance = irrandiance[ID], temp = temp_cell[ID])
    print(G.nodes.data())

    # Add parents node
    G.add_node(node_parents)

    print(G.number_of_nodes())
    print(list(G.nodes))

    G.add_edges_from(edges_list, length = 10)
    print(G.edges)
    # print(G['parent'][301])

    # Select Graph layout
    pos = nx.layout.spring_layout(G)
    # pos = nx.drawing.nx_agraph.graphviz_layout(G, prog='dot')
    plt.title("draw_networkx")
    # # Drawing nodes
    # nodes = nx.draw_networkx_nodes(G, pos, node_size=20, node_color='Green')
    # # Drawing edges
    # edges = nx.draw_networkx_edges(G, pos, node_size=20, arrowstyle='->',
    #                                arrowsize=10, edge_cmp=plt.cm.Greys,
    #                                width=1.5)

    # draw network
    # network = draw.Draw_network(G, pos)
    # network.draw()

    # # Drawing nodes
    # nodes = nx.draw_networkx_nodes(self.G, self.pos, node_size=node_sizes, node_color='Green')
    # # Drawing edges
    # edges = nx.draw_networkx_edges(self.G, self.pos, node_size=node_sizes, arrowstyle='->',
    #                                arrowsize=10, edge_color=edge_colors,
    #                                edge_cmap=plt.cm.Greys, width=1.5)
    nx.draw_networkx(G, pos, node_size=1000, alpha=0.7, node_color="Green")

    plt.show()






