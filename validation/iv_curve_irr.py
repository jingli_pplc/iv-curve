"""
Plot IV curve
STC condition:
    irradiation = 800-1000 w/m2
    cell temperature = 25 C

"""
from ivmodels import cell_irr
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

"""
Depends on cell to module device
"""
cell_surface = 0.155 ** 2 - 0.015 ** 2 * 2
cell_number = 60

"""
STC condition:

"""
# stc_radiation = np.arange(200, 1200, 200)
# cell_radiation = stc_radiation * cell_surface
# cell_temperature = 25 + 273.15
stc_radiation = np.arange(200, 1200, 200)
cell_radiation = stc_radiation * cell_surface
cell_temperature = 25 + 273.15


"""
Plot range
"""
module_voltage = np.arange(0, 50, 0.01)
cell_voltage = (module_voltage / cell_number)

"""
Optimized parameters
"""
# best_irr = 0.37
# best_Is = 7e-17  # Micro tuning
# best_m = 1.27
# best_a = 0.37600711169839673
# best_b = 1.329766881400525e-06
# best_Is = 9.965449773031661e-17  # Micro tuning
# best_m = 1.28802554702188

# best_a = 0.3700461477571439
# best_b = 9.45189568933961e-08
# best_Is = 6.217468247318383e-17
# best_m = 1.359533892537633
# {'Is': 5.642875874725294e-17, 'a': 0.33045040306332124, 'b': 0.00014010532995675364, 'm': 1.1954828621270672}
best_a = 0.33045040306332124
best_b = 0.00014010532995675364
best_Is = 5.642875874725294e-17
best_m = 1.1954828621270672
"""
y: module voltage
x: module current (since the cells are series connected, so cell current = module current)
Plot IV curve
"""
y = []
Voc_list = []
Isc_list = []

for cell_irradiance in cell_radiation:
    current_estimator = cell_irr.IV_irr(cell_irradiance, cell_temperature, cell_voltage,
                                best_a, best_b, best_Is, best_m)

    Voc = cell_irr.Voc(cell_irradiance, cell_temperature, best_a, best_b, best_Is, best_m)
    Isc = cell_irr.Isc(cell_irradiance, cell_temperature, best_a, best_b)
    Isc_list.append(Isc)
    Voc_list.append(Voc)
    y.append(current_estimator)
print(Isc_list, Voc_list)

plt.scatter(Voc_list, Isc_list)
plt.show()
fig = plt.figure()
x = module_voltage
ax = fig.add_subplot(111)
for i in range(0, len(stc_radiation)):
    ax.scatter(x, y[i], label=str(stc_radiation[i]) + " w/m2")
    # ax.set_title("IV_curve under STC condition")
    ax.set_title("IV_curve measure VS modeling")
    ax.set_xlabel("Voltage")
    ax.set_ylabel("Current")
    ax.set_xlim((0, 50))
    ax.set_ylim((0, 10))
    ax.legend(loc='upper right')
plt.show()
