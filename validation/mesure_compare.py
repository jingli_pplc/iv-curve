"""
Plot IV curve
STC condition:
    irradiation = 800-1000 w/m2
    cell temperature = 25 C

"""
from ivmodels import cell_irr
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

"""
Read CSV file
"""
df = pd.read_csv("measureIV.csv", names=["Volatage", "Current"])
print(df)

"""
Depends on cell to module device
"""
cell_surface = 0.155 ** 2 - 0.015 ** 2 * 2
cell_number = 60

"""
STC condition:

"""
# stc_radiation = np.arange(200, 1200, 200)
# cell_radiation = stc_radiation * cell_surface
# cell_temperature = 25 + 273.15
# stc_radiation = np.arange(200, 1200, 200)
stc_radiation = np.arange(830, 835, 5)
cell_radiation = stc_radiation * cell_surface
# cell_temperature = 25 + 273.15
cell_temperature = 62.35 + 273.15

"""
Plot range
"""
module_voltage = np.arange(0, 50, 0.01)
cell_voltage = (module_voltage / cell_number)

"""
Optimized parameters
"""
# best_irr = 0.37
# best_Is = 7e-17  # Micro tuning
# best_m = 1.27
# best_a = 0.37600711169839673
# best_b = 1.329766881400525e-06
# best_Is = 9.965449773031661e-17  # Micro tuning
# best_m = 1.28802554702188

# best_a = 0.3700461477571439
# best_b = 9.45189568933961e-08
# best_Is = 6.217468247318383e-17
# best_m = 1.359533892537633
# train by 9/08
# {'Is': 5.642875874725294e-17, 'a': 0.33045040306332124, 'b': 0.00014010532995675364, 'm': 1.1954828621270672}
# best_a = 0.33045040306332124
# best_b = 0.00014010532995675364
# best_Is = 5.642875874725294e-17
# best_m = 1.1954828621270672
# homo train
# {'Is': 8.521215213165306e-18, 'a': 0.3340333412312549, 'b': 0.00011721375343076593, 'm': 1.1230145008409234}
# best_a = 0.3340333412312549
# best_b = 0.00011721375343076593
# best_Is = 8.521215213165306e-18
# best_m = 1.1230145008409234
# radiation > 500
# # {'Is': 2.6439612030972894e-17, 'a': 0.3730843150471778, 'b': 8.003361319272273e-06, 'm': 1.1604664289104558}
# best_a = 0.3730843150471778
# best_b = 8.003361319272273e-06
# best_Is = 2.6439612030972894e-17
# best_m = 1.1604664289104558
# radiation > 700
# {'Is': 7.400558951066888e-17, 'a': 0.3365471032740415, 'b': 0.0001240147202015076, 'm': 1.2275646146871642}
# {'Is': 3.785005528508333e-17, 'a': 0.3138869845755221, 'b': 0.00018232228412103145, 'm': 1.2088514659443654}
best_a = 0.3138869845755221
best_b = 0.00018232228412103145
best_Is = 3.785005528508333e-17
best_m = 1.2088514659443654
# radiation > 800
#{'Is': 8.597894742445975e-17, 'a': 0.25782200910144365, 'b': 0.0003651325849538205, 'm': 1.2297826330602843}
# best_aa = 0.25782200910144365
# best_bb = 0.0003651325849538205
# best_Iss = 8.597894742445975e-17
# best_mm = 1.2297826330602843
# cross train
# {'Is': 9.063592951808672e-17, 'a': 0.3402609500757599, 'b': 0.0004580311762816981, 'm': 1.3018173785129414}
# {'Is': 1.6074677825787333e-17, 'a': 0.15870740478143094, 'b': 0.000653329505383669, 'm': 1.1018832022778298}
best_aa = 0.15870740478143094
best_bb = 0.000653329505383669
best_Iss = 1.6074677825787333e-17
best_mm = 1.1018832022778298
"""
y: module voltage
x: module current (since the cells are series connected, so cell current = module current)
Plot IV curve
"""
y = []
Voc_list = []
Isc_list = []

for cell_irradiance in cell_radiation:
    current_estimator = cell_irr.IV_irr(cell_irradiance, cell_temperature, cell_voltage,
                                best_a, best_b, best_Is, best_m)
    Voc = cell_irr.Voc(cell_irradiance, cell_temperature, best_a, best_b, best_Is, best_m)
    Isc = cell_irr.Isc(cell_irradiance, best_a, best_b, cell_temperature)
    Isc_list.append(Isc)
    Voc_list.append(Voc)
    y.append(current_estimator)
print(Isc_list, Voc_list)

"""
Plot comparing IV curve
"""
yy = []
Voc_list_2 = []
Isc_list_2 = []

for cell_irradiance in cell_radiation:
    current_estimator = cell_irr.IV_irr(cell_irradiance, cell_temperature, cell_voltage,
                                best_aa, best_bb, best_Iss, best_mm)
    Voc = cell_irr.Voc(cell_irradiance, cell_temperature, best_aa, best_bb, best_Iss, best_mm)
    Isc = cell_irr.Isc(cell_irradiance, best_aa, best_bb, cell_temperature)
    Isc_list_2.append(Isc)
    Voc_list_2.append(Voc)
    yy.append(current_estimator)

plt.scatter(Voc_list, Isc_list)
plt.show()
fig = plt.figure()
x = module_voltage
ax = fig.add_subplot(111)
for i in range(0, len(stc_radiation)):
    ax.scatter(x, y[i], label="One day " + str(stc_radiation[i]) + " w/m2")
    ax.scatter(x, yy[i], label="Two month " + str(stc_radiation[i]) + " w/m2")
    # ax.set_title("IV_curve under STC condition")
    ax.set_title("IV_curve measure VS modeling")
    ax.set_xlabel("Voltage")
    ax.set_ylabel("Current")
    ax.set_xlim((0, 50))
    ax.set_ylim((0, 10))
    ax.legend(loc='upper right')
ax.scatter(x=df["Volatage"], y=df["Current"])
plt.show()



