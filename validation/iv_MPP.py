"""
Plot MPP curve
STC condition:
    irradiation = 1000 w/m2
    cell temperature = 25 C

"""
from ivmodels import cell, constant
import matplotlib.pyplot as plt
import numpy as np

"""
Depends on cell to module device
"""
cell_surface = 0.1524 ** 2
cell_number = 60

"""
STC condition:

"""
stc_radiation = 1000
cell_radiation = stc_radiation * cell_surface
cell_temperature = 25 + 273.15

"""
Plot range
"""
module_voltage = np.arange(0, 50, 0.01)
cell_voltage = module_voltage / cell_number
cell_voltage = cell_voltage
"""
Optimized parameters
"""
best_irr = 0.37
best_Is = 7.08e-17
best_m = 1.41
"""
y: module voltage
x: module current (since the cells are series connected, so cell current = module current)
Plot IV curve
"""
y = []
P_max = 0
for voltage in cell_voltage:
    current_estimator = cell.IV(cell_radiation, cell_temperature, voltage,
                                best_irr, best_Is, best_m)
    pp = voltage * cell_number * current_estimator
    if pp > P_max:
        P_max = pp
        best_parameters = {'Vpm': voltage*cell_number, 'Ipm': current_estimator}

print(best_parameters)
print("MPP_module: {}".format(P_max))

"""
Efficiency
"""

cell_efficiency = P_max/cell_number/1000/cell_surface
print("cell_efficiency: {}".format(cell_efficiency))

# fig = plt.figure()
# x = module_voltage
# ax = fig.add_subplot(111)
# for i in range(0, len(stc_radiation)):
#     ax.scatter(x, y[i], label=str(stc_radiation[i]) + " w/m2")
#     ax.set_title("IV_curve under STC condition")
#     ax.set_xlabel("Voltage")
#     ax.set_ylabel("Current")
#     ax.set_xlim((0, 50))
#     ax.set_ylim((0, 10))
#     ax.legend(loc='upper right')
# plt.show()

