import numpy as np
import matplotlib.pyplot as plt
import data.data_influx as flux
import pandas as pd
"""
himawari data
"""
start = -1
temp = [1.97,1.83,1.76,1.86,1.72,1.27,0.68,0.45,0.07,
        0,0,0,0,0,0,0,0,0,0,0,0.04,0.41,0.86,1.22,1.61,
        1.97,2.57,2.87,2.59,2.13,1.51,0.8,0.1,0,0,0,0,0,0,0,0,0,0,0,0.06,0.55,1.08]
hourtime = []
for t in range(0, len(temp)):
    hour = start + t
    hourtime.append(hour)
print(hourtime)
print(len(hourtime))
himawari = np.array(temp)
print(himawari)
print(len(himawari))

"""
Measured data from sensor
"""
# irradiance = flux.radiation #unit w/m2 -> wj/
# unit w/m2 -> wj/m2
# print(len(irradiance))
data = []
with open("data/radiation.csv") as f:
    for line in f:
        parts = line.rstrip("\n").split(';')
        data.append(parts)

# df = pd.read_csv("data/radiation.csv")
# irradiance = df.isnull().sum()
print(data)
radiation = pd.DataFrame(data[1:], columns=['Time', 'radiation'])
print(radiation)
radiation["radiation"] = radiation["radiation"].astype(float)
radiation["radiation"] = radiation["radiation"]/1e6
radiation["radiation"].plot()

plt.scatter(hourtime, himawari, color='green')

plt.xlabel("Hour")
plt.ylabel("Iraddiation MJ/m2")
# plt.scatter(hourtime, irradiance)
plt.show()

"""kishocho comparing"""
radiation_sensor = flux.radiation
radiation_satellite = flux.radiation_satellite
ax = radiation_sensor.plot()
radiation_satellite.plot(ax=ax)
plt.xlabel("Radiation")
ax.legend(["Sensor", "Satellite"])
plt.show()

