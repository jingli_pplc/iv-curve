from ivmodels import cell_irr
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import data.test_dataset as test

#{'Is': 6.217468247318383e-17, 'a': 0.3700461477571439, 'b': 9.45189568933961e-08, 'm': 1.359533892537633}
# best_a = 0.3289835127645533
# best_b = 0.00014394059259259848
# best_Is = 5.0181799229437715e-17
# best_m = 1.1861114532401378
# best_a = 0.3700461477571439
# best_b = 9.45189568933961e-08
# best_Is = 6.217468247318383e-17
# best_m = 1.359533892537633
# homo train
# {'Is': 8.521215213165306e-18, 'a': 0.3340333412312549, 'b': 0.00011721375343076593, 'm': 1.1230145008409234}
best_a = 0.3340333412312549
best_b = 0.00011721375343076593
best_Is = 8.521215213165306e-18
best_m = 1.1230145008409234
# cross train
# #{'Is': 5.855461783937959e-17, 'a': 0.3527469690175966, 'b': 0.00023760795194747506, 'm': 1.3583559907811251}
# best_a = 0.3527469690175966
# best_b = 0.00023760795194747506
# best_Is = 5.855461783937959e-17
# best_m = 1.3583559907811251

"""
Measured data from sensor
"""
irradiance = test.radiation #unit w/m2

# irradiance.plot()
# plt.show()
cell_temperature = test.temperature_cell + 273.15
# cell_temperature.plot()
# plt.show()
module_voltage = test.voltage
# module_voltage.plot()

module_current = test.current
# module_current.plot()
# plt.show()

"""
Device related
series collection of cells
"""
#Todo move this part to panel.py file
cell_number = 60
cell_surface = 0.1524**2
cell_irradiance = cell_surface*irradiance
cell_voltage = module_voltage / cell_number  # measure data from database voltage[id][t]
cell_voltage = cell_voltage*1
cell_current = module_current # current[id][t]

"""
plot the best parameters results
"""
best_estimator = cell_irr.IV_irr(cell_irradiance["radiation"], cell_temperature["Cell_temperature"],
                                        cell_voltage["voltage"],
                                        best_a, best_b, best_Is, best_m)
erro = mean_squared_error(best_estimator, cell_current)
print("mean squre erro:{}".format(erro))
ax = cell_current.plot()
best_estimator.plot(ax=ax)
plt.xlabel("Current")
ax.legend(["Measured current", "Forecast current", "E_Current(only radiation data)"])
plt.show()