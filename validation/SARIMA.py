from ivmodels import cell_irr
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import numpy as np
import data.data_influx as flux
from hyperopt import fmin, hp, tpe, anneal, Trials
from hyperopt.mongoexp import MongoTrials
import data.test_dataset as test
import statsmodels.api as sm

"""
Panel number
"""
"""
Given parameters
"""

max_evals = 500

"""
Satellite data
"""
irradiance = flux.radiation_satellite #unit w/m2

cell_temperature = flux.temperature_satellite + 273.15# measure data from database cell_temperature[id][t]
"""
Measured data from sensor
"""
module_voltage = flux.voltage

module_current = flux.current

"""
hyper-parameter space
"""
space = {
    'p_values': hp.choice('p_values', range(0, 5, 1)),
    'd_values': hp.choice('d_values', range(0, 5, 1)),
    'q_values': hp.choice('q_values', range(0, 5, 1)),
    'P_values': hp.choice('P_values', range(0, 5, 1)),
    'D_values': hp.choice('D_values', range(0, 5, 1)),
    'Q_values': hp.choice('Q_values', range(0, 5, 1)),
    'period': hp.choice('period', range(0, 5, 1))
}
"""
Device related
series collection of cells
"""
#Todo move this part to panel.py file
#Todo check the surface area of cell
cell_number = 60
cell_surface = 0.155**2-0.015**2*2
cell_irradiance = cell_surface*irradiance
cell_voltage = (module_voltage / cell_number)# voltage loss duel to cell series collection
cell_current = module_current # current[id][t]

"""
Train and test
"""
train = cell_current["current"]
co_train = cell_voltage["voltage"]
test_data = test.current["current"]
co_test = test.voltage["voltage"]/cell_number

"""
use cell_voltage to predict cell current and compared with measure data
estimator: cell_current, Voc, Iph
Erro measured by RMSE
Array-like input
"""


def objective(parameters):
    p_values = parameters["p_values"]
    d_values = parameters["d_values"]
    q_values = parameters["q_values"]
    P_values = parameters["P_values"]
    D_values = parameters["D_values"]
    Q_values = parameters["Q_values"]
    period = parameters["period"]
    arima_order = (p_values, d_values, q_values)
    seasonal_order = (P_values, D_values, Q_values, period)
    model = sm.tsa.SARIMAX(train, order=arima_order,
                                       seasonal_order=seasonal_order, exog=co_train)
    model_fit = model.fit()
    current_estimator = model_fit.predict(
        start = len(train),
        end = len(train) + len(co_test) -1,
        exog=co_test,
        dynamic=False
    )


        # current_estimator= cell_irr.IV_irr(cell_irradiance["radiation"], cell_temperature["temperature"],
        #                                 cell_voltage["voltage"],
        #                                 a, b, Is, m)
    loss = mean_squared_error(cell_current, current_estimator)
    return loss


# # trials = MongoTrials('mongo://127.0.0.1:27017/local/jobs', exp_key='exp1')
# best = fmin(objective, space, trials=trials, algo=tpe.suggest, max_evals = max_evals)

best = fmin(objective, space, algo=anneal.suggest, max_evals = max_evals)
print(best)

"""
plot the best parameters results
"""
# best_m = best.get('m')
# best_a = best.get('a')
# best_b = best.get('b')
# best_Is = best.get('Is')
# print(best_a, best_b, best_Is, best_m)



