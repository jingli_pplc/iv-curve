from ivmodels import cell_irr
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import numpy as np
import data.data_influx as flux

"""
Panel number
"""
"""
Given parameters
"""
# ID = "'8'"
# start_time = '2019-08-31 05:00:00'
# end_time = '2019-09-03 18:00:00'
# time_step = "1h"


"""
Measured data from sensor
"""
irradiance = flux.radiation #unit w/m2

# irradiance.plot()
# plt.show()
cell_temperature = flux.temperature_cell + 273.15# measure data from database cell_temperature[id][t]
# cell_temperature.plot()
# plt.show()
module_voltage = flux.voltage
# module_voltage.plot()

module_current = flux.current
# module_current.plot()
# plt.show()

"""
hyper-parameter space
"""
#Todo: check the accuracy of measured irradiation
# irr_range =np.arange(0.2, 0.45, 0.01)
# Is_range = np.arange(9.3E-17, 9.9E-17, 0.01E-17)
# m_range = np.arange(1.1, 1.4, 0.01)
a_range = np.arange(0.1, 0.5, 0.01)
b_range = np.arange(1E-6, 1E-3, 1E-6)
Is_range = np.arange(8.0E-17, 10.0E-17, 0.01E-17)
m_range = np.arange(1.0, 1.5, 0.01)
"""
Device related
series collection of cells
"""
#Todo move this part to panel.py file
#Todo check the surface area of cell
cell_number = 60
# cell_surface = 0.1524**2
cell_surface = 0.155**2-0.015**2*2
cell_irradiance = cell_surface*irradiance
cell_voltage = (module_voltage / cell_number)# voltage loss duel to cell series collection
cell_current = module_current # current[id][t]

"""
use cell_voltage to predict cell current and compared with measure data
estimator: cell_current, Voc, Iph
Erro measured by RMSE
Array-like input
"""
Min_erro = 1E5
for m in m_range:
    for const_Is in Is_range:
        for const_a in a_range:
            for const_b in b_range:
                current_estimator = cell_irr.IV_irr(cell_irradiance["radiation"], cell_temperature["Cell_temperature"],
                                                    cell_voltage["voltage"],
                                                    const_a, const_b, const_Is, m)
                erro = mean_squared_error(cell_current, current_estimator)
                # aa, bb = np.meshgrid(const_a, const_b, sparse=True)
                h = plt.contourf(const_a, const_b, erro)
                plt.show()
                if erro < Min_erro:
                    Min_erro = erro
                    best_parameters = {'const_Is': const_Is, 'const_a': const_a, 'const_b': const_b, 'm': m}
                    best_erro = {'best_erro': Min_erro}

print("best_parameters: {}".format(best_parameters))
print("best_erro: {}".format(best_erro))

"""
plot the best parameters results
"""
best_m = best_parameters.get('m')
best_a = best_parameters.get('const_a')
best_b = best_parameters.get('const_b')
best_Is = best_parameters.get('const_Is')
print(best_a,best_b, best_Is, best_m)
best_estimator = cell_irr.IV_irr(cell_irradiance["radiation"], cell_temperature["Cell_temperature"],
                                        cell_voltage["voltage"],
                                        best_a, best_b, best_Is, best_m)
ax = cell_current.plot()
best_estimator.plot(ax=ax)
plt.xlabel("Current")
# rad_current = cell_irradiance * best_irr
# rad_current.plot(ax=ax)
ax.legend(["Measured current", "Estimated current", "E_Current(only radiation data)"])
plt.show()








