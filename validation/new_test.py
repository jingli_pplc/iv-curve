from ivmodels import cell
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import data.test_dataset as test
"""
Panel number
ID 14
"""

"""
Measured data from sensor
"""
irradiance = test.radiation #unit w/m2

# irradiance.plot()
# plt.show()
cell_temperature = test.temperature_cell + 273.15
# cell_temperature.plot()
# plt.show()
module_voltage = test.voltage
# module_voltage.plot()

module_current = test.current
# module_current.plot()
# plt.show()

"""
Device related
series collection of cells
"""
#Todo move this part to panel.py file
cell_number = 60
cell_surface = 0.1524**2
cell_irradiance = cell_surface*irradiance
cell_voltage = module_voltage / cell_number  # measure data from database voltage[id][t]
cell_current = module_current # current[id][t]

"""
plot the best parameters results
"""
best_irr = 0.38
best_Is = 9.61e-17
best_m = 1.31
best_estimator = cell.IV(cell_irradiance["radiation"], cell_temperature["Cell_temperature"],
                                        cell_voltage["voltage"],
                                        best_irr, best_Is, best_m)
erro = mean_squared_error(best_estimator, cell_current)
print("mean squre erro:{}".format(erro))
ax = cell_current.plot()
best_estimator.plot(ax=ax)
plt.xlabel("Current")
ax.legend(["Measured current", "Validation current", "E_Current(only radiation data)"])
plt.show()