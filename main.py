"""
main file including :

Loading data : data
        based on ID, time range, main character (I, V et al)

Physical model : ivmodels
        simple model of IV related

Function approximation : calculation_ivcurve
        optimization method

Writing data : storage_data
       main characters to describe PV performance

Jing Li
2019/10/16
Tokyo
"""
import numpy as np
import storage_data.connect_mysql as save
from hyperopt import hp
import data.subset_homo as data
import calculation_ivcurve.hyper_parameters as calculate
import calculation_ivcurve.charaters as character
import plot_figure.iv_curve as iv


class IVcurve:

    def __init__(self, ID, start_time, end_time, time_step):
        self.ID = ID
        self.start_time = start_time
        self.end_time = end_time
        self.time_step = time_step

    # loading and making subset data
    def preprocess(self):
        print(self.ID)

        # setting range of data set
        low_value = 600
        high_value = 1000
        step = 100

        # instance
        homo = data.Homone(low_value, high_value, step, self.ID, self.start_time, self.end_time, self.time_step)

        # clean data set
        clean_data = homo.clean()

        # homo data set
        train_data = homo.homoset(clean_data)
        print("1, preprocess finished")
        return train_data

    # calculate the best parameters for the function
    def cal_para(self, trainset, cell_surface):
        """
        Given parameters
        """
        # training steps
        max_evals = 1000

        # device related
        cell_number = 60
        # cell_surface = 0.155 ** 2 - 0.015 ** 2 * 2

        # hyper-parameter space
        space = {
            'a': hp.uniform('a', 0.0, 1.0),
            'b': hp.uniform('b', 1E-8, 1E-3),
            "Is": hp.uniform('Is', 1.0E-19, 10.0E-17),
            "m": hp.uniform('m', 1.0, 1.5)
            # 'a': hp.uniform('a', 0.36, 0.4),
            # 'b': hp.uniform('b', 1E-8, 1E-5),
            # "Is": hp.uniform('Is', 1.0E-19, 10.0E-17),
            # "m": hp.uniform('m', 1.2, 1.25)
        }
        """
        Measured data from sensor
        """
        print(trainset)
        irradiance = trainset.radiation  # unit w/m2
        cell_temperature = trainset.Cell_temperature + 273.15  # measure data from database cell_temperature[id][t]
        module_voltage = trainset.voltage
        module_current = trainset.current

        """
        series collection of cells
        """
        cell_irradiance = cell_surface * irradiance
        cell_voltage = (module_voltage / cell_number)  # voltage loss duel to cell series collection
        cell_current = module_current  # current[id][t]

        """
        best parameters calculation
        """
        Bestpara = calculate.Bestpara(radiation=cell_irradiance, temperature=cell_temperature, voltage=cell_voltage,
                                      cell_current=cell_current)

        optimized_para = Bestpara.best(Bestpara.objective, space, max_evals, do_show=False)
        print("2, calculate finished")
        return optimized_para

    # writing data
    def write(self, radiation, temperature, power_max, Voc, Iph, FF, Best_Is, m, a, b):
        # query = "INSERT INTO pvcurve (ID, radiation, temperature,power_max, Voc, Iph, FF) "
        query = "INSERT INTO pvcurve2 (ID, radiation, temperature, power_max, Voc, Iph, FF, Best_Is, m, a, b) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        panel_id = int(self.ID)
        # val = (panel_id, 1000, 298, m, a, b, Is)
        val = (panel_id, radiation, temperature, power_max, Voc, Iph, FF, Best_Is, m, a, b)
        data = save.Datago()
        data.write(query, val)


if __name__ == '__main__':
    # loading panel ID list
    # [1,321) = (1......,320)
    ID_list = np.arange(8, 9)

    # Giving parameters
    start_time = '2019-08-31 00:00:00'
    end_time = '2019-10-14 23:59:00'
    time_step = "5m"

    # Device related
    cell_surface = 0.155 ** 2 - 0.015 ** 2 * 2
    cell_number = 60

    # STC test condition
    radiation = 1000
    temperature = 298.15

    # testing radiation range and temperature range
    radiation_range = np.arange(400, 1200, 200)
    temperature_range = np.arange(298.15, 398.15, 20)

    for ID in ID_list:
        ins = IVcurve(ID, start_time, end_time, time_step)
        # preparing for train data
        trainset = ins.preprocess()
        # calculate the iv curve parameters
        para = ins.cal_para(trainset, cell_surface)

        """
        Get the best parameters results
        """
        # m = para.get('m')
        # a = para.get('a')
        # b = para.get('b')
        # Is = para.get('Is')

        a = 0.3697344628453642
        b = 1.9005289913981644e-05
        Is = 2.8131557624564604e-17
        m = 1.1553139682042382

        # calculate the main characters: Ish, voc, vpm, ipm, mpp, ff
        chara = character.IshVoc(radiation, temperature, cell_surface, cell_number, Is, m, a, b)
        vpmipm = chara.vpmipm()
        Ish = float(vpmipm.get('Ish'))
        voc = float(vpmipm.get('Voc'))
        vpm = float(vpmipm.get('Vpm'))
        ipm = float(vpmipm.get('Ipm'))
        mpp = float(vpmipm.get('MPP_module'))
        ff = float(vpmipm.get('FF'))
        print(radiation, temperature, mpp, voc, Ish, ff, vpm, ipm, Is, m, a, b)

        # writing results to database
        ins.write(radiation, temperature, mpp, voc, Ish, ff, Is, m, a, b)

        # plot and save iv curve figure to local path
        figure_plot = iv.CurvePlot(ID, cell_number, cell_surface, radiation_range, temperature_range,
                                   radiation, temperature, a, b, Is, m)

        # radiation: stc temperature and radiation range
        # temperature: stc radiation and temperature range
        figure_plot.iv_figure("radiation", plotCSV=True)
        figure_plot.iv_figure("temperature", plotCSV=True)

        # Write data to influxdb
