"""
iv-curve
input: a, b, Is, m
Methods:
# type radiation
1, Voc Isc figure_ radiation difference, temperature constant
input: Voltage range, radiation range
output: Current
# type temperature
2, Voc Isc figure _ temperature difference, radiation constant
input: Voltage range, temperature range
output: Current
figure: x = voltage, y = current

"""

import numpy as np
from ivmodels import cell_irr
import matplotlib.pyplot as plt
import os
import visu.con_influx as tube
import csv


class CurvePlot:

    def __init__(self, ID, cell_number, cell_surface, radiation_range, temperature_range,  stc_rad,
                 stc_temp, a, b, Is, m):
        self.ID = ID
        self.cell_number = cell_number
        self.cell_surface = cell_surface
        self.radiation = radiation_range
        self.temperature = temperature_range
        self.stc_temperature = stc_temp # default value
        self.stc_radiation = stc_rad # default value
        self.a = a
        self.b = b
        self.Is = Is
        self.m = m

    def iv_figure(self, type, plotCSV=False):

        """
        Plot range
        """
        module_voltage = np.arange(0, 50, 0.01)
        # series collection
        cell_voltage = (module_voltage / self.cell_number)

        """
        cell voltage -> cell current
        """
        # current list
        y = []
        if type == "radiation":
            cell_radiation = self.radiation * self.cell_surface
            cell_temperature = self.stc_temperature
            loop_value = cell_radiation

            for item in loop_value:
                current_estimator = cell_irr.IV_irr(item, cell_temperature, cell_voltage,
                                                    self.a, self.b, self.Is, self.m)
                y.append(current_estimator)
        elif type == "temperature":
            cell_radiation = self.stc_radiation * self.cell_surface
            cell_temperature = self.temperature
            loop_value = cell_temperature

            for item in loop_value:
                current_estimator = cell_irr.IV_irr(cell_radiation, item, cell_voltage,
                                                    self.a, self.b, self.Is, self.m)
                y.append(current_estimator)
        else:
            print("Wrong input type")
        x = module_voltage
        fig = plt.figure()
        ax = fig.add_subplot(111)

        # path of csv files
        path_csv = 'storage_data'
        if type == "radiation":
            for i in range(0, len(self.radiation)):
                ax.scatter(x, y[i], label=str(self.radiation[i]) + " Wperm2  " + str(self.stc_temperature)+ " K")
                name = os.path.join(path_csv, 'ID' + str(self.ID)+'_radiation_csv')
                if plotCSV:
                    try:
                        path = os.mkdir(name)
                    except FileExistsError:
                        pass
                    # Write data to CSV file
                    with open(name + '/'+ str(self.radiation[i]) + 'Wperm2.csv', 'w', newline='') as myfile:
                        header = ["Voltage", "Current"]
                        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
                        wr.writerow(header)
                        rows = zip(x, y[i])
                        for row in rows:
                            wr.writerow(row)

        elif type == "temperature":
            for i in range(0, len(self.temperature)):
                ax.scatter(x, y[i], label=str(self.stc_radiation) + " w/m^2  " + str(self.temperature[i]) + " K")
                if plotCSV:
                    name = os.path.join(path_csv,'ID' + str(self.ID)+'_temperature_csv')
                    if plotCSV:
                        try:
                            path = os.mkdir(name)
                        except FileExistsError:
                            pass
                    # Write data to CSV file
                    with open(name + '/'+ str(self.temperature[i]) + 'K_file.csv', 'w', newline='') as myfile:
                        header = ["Voltage", "Current"]
                        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
                        wr.writerow(header)
                        rows = zip(x, y[i])
                        for row in rows:
                            wr.writerow(row)
        else:
            print("Check the range of radiation or temperature!")
        ax.set_title("IV_curve Prediction")
        ax.set_xlabel("Voltage [V]")
        ax.set_ylabel("Current [A]")
        ax.set_xlim((0, 50))
        ax.set_ylim((0, 10))
        ax.legend(loc='lower left')


        # plt.show()

        # Saving figures to local path
        save_path = os.path.dirname(__file__)
        save_file = "ID_" + str (self.ID) + "_" + type + '.jpg'
        ...
        fig.savefig(os.path.join(save_path, save_file))

# if __name__== "__main__":
#     # Device related
#     cell_surface = 0.155 ** 2 - 0.015 ** 2 * 2
#     cell_number = 60
#     stc_rad = 1000
#     stc_tem = 298
#     ID = 8
#     best_a = 0.3340333412312549
#     best_b = 0.00011721375343076593
#     best_Is = 8.521215213165306e-18
#     best_m = 1.1230145008409234
#     # testing radiation range and temperature range
#     radiation_range = np.arange(400, 1200, 200)
#     temperature_range = np.arange(298.15, 398.15, 20)
#
#     ins = CurvePlot(ID, cell_number, cell_surface, radiation_range, temperature_range,stc_rad, stc_tem,
#                     best_a, best_b, best_Is, best_m)
#     ins.iv_figure("temperature")