"""
Plot iv time series figures to calculate the erro
Raw data & Clean data
1, Time series Validation
input: train start time, end time, radiation, temperature, voltage, a, b, Is,m
output: Current time series figure
2, Time series forecasting
input: forecast start time, end time
output: Current time series figure
3, write to influxdb
"""

from ivmodels import cell_irr
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import data.subset_homo as source
import data.write_influxdb as kafka
import pandas as pd



class Validation_plot:

    def __init__(self, start, end, step, cell_number, cell_surface):
        # self.a = a
        # self.b = b
        # self.Is = Is
        # self.m = m
        self.start = start
        self.end = end
        self.step = step
        # self.data = data
        self.cell_number = cell_number
        self.cell_surface = cell_surface

    """
    1,Return time series graph
    2,calculate erro 
    3,writing erro to database
    4,Save figure as local files
    """
    def val_graph(self, type):

        """prepare the data required: raw data or clean data"""
        if type == "raw":
            # Directly from database
            df = self.data

        # elif type == "clean":
        #     # use homo.clean to clean the wrong data

        else:
            print("Please write the input data type!")

        voltage = df["voltage"]
        # check the temperature name
        temperature = df["temperature"]

        """device related:
        from sensor to cell level
        """


        """ start-end range time series real current """

        """ start-end range time series simulate (prediction/forecast) current """
        # return {"real": real_current, "simulate": simulate_current}

        """
        Plot time series
        """

        best_estimator = cell_irr.IV_irr(cell_irradiance, cell_temperature,
                                         cell_voltage,
                                         self.a, self.b, self.Is, self.m)
        erro = mean_squared_error(best_estimator, cell_current)
        print("mean squre erro:{}".format(erro))
        ax = cell_current.plot()
        best_estimator.plot(ax=ax)
        plt.xlabel("Current")
        ax.legend(["Measured current", "Forecast current", "E_Current"])
        plt.show()

    def write_article(self):
        # setting range of data set
        low_value = 0
        high_value = 2000
        step_value = 100
        ID = "8"

        # time range
        # start_time = '2019-08-31 00:00:00'
        # end_time = '2019-10-14 23:59:00'
        # time_step = "5m"
        # instance
        if type == "clean":
            data = source.Homone(low_value, high_value, step_value, ID, self.start, self.end, self.step)
            # clean data set
            clean_data = data.clean()
            print(clean_data.columns)



if __name__ == "__main__":
    a = 0.33045040306332124
    b = 0.00014010532995675364
    Is = 5.642875874725294e-17
    m = 1.1954828621270672

    # Device related
    cell_surface = 0.155 ** 2 - 0.015 ** 2 * 2
    cell_number = 60

    # setting range of data set
    low_vali = 0
    high_vali = 2000
    step_vali = 100
    ID = "8"

    # time range
    start_vali = '2019-08-31 00:00:00'
    end_vali = '2019-10-14 23:59:00'
    time_vali = "5m"

    ins = Validation_plot(start_vali, end_vali, step_vali, cell_number, cell_surface)
    ins.write_article()



    # # instance
    # homo = source.Homone(low_value, high_value, step, ID, start_time, end_time, time_step)
    #
    # # clean data set
    # clean_data = homo.clean()
    # print(clean_data.columns)


    # df = pd.DataFrame()
    # df["simulate"] = clean_data["current"]
    # df.index = pd.to_datetime(df.index)
    # print(df)
    # # df = clean_data["current"]
    #
    # # write to influxdb
    #
    # ins = kafka.Shakespeare(df, 'normalized_field12')
    # ins.write_current()










