from data import con_influxdb
import matplotlib.pyplot as plt


field = "normalized_field12"
col = "temperature"
ID = "'8'"
start_time = "'2019-04-16 00:00:01'"
end_time = "'2019-05-16 00:00:01'"
time_step = "1h"
start = "2019-08-09 11:00:00+00:00"
end = "2019-08-10 11:00:00+00:00"
Box_temp = con_influxdb.loadata(field, col, ID, start_time, end_time, time_step)
cell_temp = con_influxdb.load_envi(col, start, end, time_step)
print(cell_temp)

cell_temp.plot()
plt.show()
