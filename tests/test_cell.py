from ivmodels import cell_function
from ivmodels import constant
import matplotlib.pyplot as plt
import pandas as pd

"""
TEST
"""
test = [(2012, 5), (2013, 6)]
df_test = pd.DataFrame(test)
# print(df_test)
irradiance = df_test.set_index([0])
# print(irradiance)


"""
Device related
"""
cell_number = 60

"""
Meatured data from sensor
"""
#TOdo irradiance is from database: meatured by sensor
# irradiance = 20  # measure data from database  irradiance[t]

# Kelve K
#Todo cell temperature is from database: meatured by sensor
cell_temperature = 300 # measure data from database cell_temperature[t]


# Todo module voltage is from database: measured by sensor
module_voltage = 31

"""
Irradiance related
"""
#Todo tuning const_irr range is (0, 1)
const_irr = 0.2 # tuning data
Iph = cell_function.photo_current(const_irr, irradiance) # Iph[t]

"""
material related 
"""
N_material = constant.N_material
bandgap = constant.bandgap()


"""
cell temperature related
"""

free_electrons = cell_function.free_electron(N_material, bandgap, cell_temperature, constant.thermo_k) # unit: cm-3
#Todo tuning const_Is (range??)
const_Is = 5E-10 # turning parameter
# cell_T -> Is[t] tuning data or physical model from satu_current
Is = cell_function.satu_current_simple(free_electrons, const_Is)
thermo_voltage = cell_function.thermal_voltage(constant.thermo_k, cell_temperature, constant.q)


"""
IV curve
"""
Voc = cell_function.Voc(constant.m, thermo_voltage, Iph, Is)

cell_voltage = module_voltage/cell_number # measure data from database voltage[t]
cell_current = cell_function.current(Iph, Is, constant.m, cell_voltage, thermo_voltage)


"""
Plot IV
"""
print(free_electrons, cell_voltage, Iph, cell_current, Is, thermo_voltage)
# print(volatage, current_cell, Iph, Is, Voc)
# plt.plot(cell_voltage, cell_current, 'o', color='green')
# plt.show()
