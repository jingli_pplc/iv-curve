# How to run:

1, pip3 install poetry

2, poetry install

3, set up your local/remote mysql database

# Main plot results
Different radiation                   |  Different temperature
:------------------------------------:|:-------------------------:
![](storage_data/ID_8_radiation.jpg)  |  ![](storage_data/ID_8_temperature.jpg)



# Main files including :

## Loading data : data
        Based on ID, time range, main character (I, V et al)

## Physical model : ivmodel
        Simple model of IV related

## Function approximation : calculation_ivcurve
        Optimization method

## Writing data : storage_data
       Main characters to describe PV performance      
       IV curve figures to path

## Saving iV curve figures : plot_figure
       Plot iv curve figures in:
       STC condition,
       different temperature condition,
       different radiation condition       