from data import con_influxdb
"""
Radiation and temperature data
train data
"""

ID = "'8'"
start_time = '2019-09-24 10:00:00'
end_time = '2019-09-25 00:59:00'
data_type = ""
time_step = "1h"
location = "'東京'"
site = location

"""
Radiation
"""
field = "jma"
col = "radiation"
radiation_satelite = con_influxdb.load_satellite(field, col, site, start_time, end_time, time_step)
print(radiation_satelite)