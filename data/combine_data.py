"""
Homogeneous Data set based on different range of radiation
"""

import pandas as pd
import data.data_influx as raw
import numpy as np

"""
Given parameters of time range
"""
start = "07:00:00"
end = "17:00:00"

# Load main features
radiation_raw = raw.radiation
temperature_raw = raw.temperature_cell
I_raw = raw.current
V_raw = raw.voltage
Box_raw = raw.temperature_box

# Combine all features to a data set
df = pd.concat([radiation_raw, temperature_raw, I_raw, V_raw, Box_raw], axis=1)
df.index = pd.to_datetime(df.index)
train_data = df.between_time(start_time=start, end_time=end)

# sample set total description
print(train_data.describe())

"""
Setting the data frame format as same as the raw data
"""
radiation = pd.DataFrame(train_data["radiation"], columns=["radiation"])
temperature_cell = pd.DataFrame(train_data["Cell_temperature"], columns=["Cell_temperature"])
voltage = pd.DataFrame(train_data["voltage"], columns=["voltage"])
current = pd.DataFrame(train_data["current"], columns=["current"])
Box_temperature = pd.DataFrame(train_data["Box_temperature"], columns=["Box_temperature"])

print(radiation.count())

