"""
Homo sub set based on different range of radiation
Select the biggest data number for all the ranges
"""
import data.data_influx as raw
import pandas as pd
import numpy as np
import data.db_influx as flux


class Homone:

    def __init__(self, low_value, high_value, step, ID, start_time, end_time, time_step):
        self.low = low_value
        self.high = high_value
        self.step = step
        self.ID = ID
        print(self.low, self.high, self.step, self.ID)
        self.start_time = start_time
        self.end_time = end_time
        self.data_type = ""  # default
        self.time_step = time_step
        self.GWID = "'GWB827EBDF6396'"  # default
        self.site = "'東京'" # default

    """
    Clean data
    input: raw data based on ID
    output: clean data set- total
    """
    def clean(self):

        # Load main features based on ID

        number = "'" + str(self.ID) + "'"

        ins = flux.pplc(number, self.start_time, self.end_time, self.data_type, self.time_step, self.GWID, self.site)

        df = ins.load_ground()

        # Load main features
        radiation_raw = df.get("radiation")
        temperature_raw = df.get("temperature_cell")
        I_raw = df.get("current")
        V_raw = df.get("voltage")
        Box_raw = df.get("temperature_box")

        # Combine all features to a dataset
        df = pd.concat([radiation_raw, temperature_raw, I_raw, V_raw, Box_raw], axis=1)
        total = df.query("25 < voltage < 32 and current > 0 and Cell_temperature > 0")

        # sample set total description
        # print(total.describe())

        return total

    """
    Making homo subset
    input: clean data set
    output : one big homo train data set
    """
    def homoset(self, clean):
        """
        Inital subset conditions based on radiation: low, high, step
        """
        value_list = np.arange(self.low, self.high, self.step)
        str_rad = [str(a) for a in value_list]
        list_len = len(str_rad)

        # Initial list for saving sub-data
        subls = []
        # Initial number for finding the smallest size of subset
        number_sub = 1E6
        # Initial empty dataframe
        train_data = pd.DataFrame()

        """
        Subset for training based on radiation
        Finding the smallest subset size
        """
        for i in np.arange(0, list_len):
            if value_list[i] < (self.high - self.step):
                df = clean.query(str_rad[i] + " <= radiation < " + str_rad[i + 1])
            elif value_list[i] >= (self.high - self.step):
                df = clean.query("radiation >= " + str_rad[i])
            subls.append(df)
            data_number = df["radiation"].count()
            if data_number < number_sub:
                number_sub = data_number

        print(number_sub)
        """
        Making a homogenious subset based on the smallest subset size
        """
        for i in np.arange(0, list_len):
            subls[i] = subls[i].sample(n=number_sub)
            train_data = train_data.append(subls[i])

        return train_data

    """
    Preparing data frame for training
    input: homo data set
    output: radiation et al
    """
    def character(self, train_data):
        radiation = pd.DataFrame(train_data["radiation"], columns=["radiation"])
        temperature_cell = pd.DataFrame(train_data["Cell_temperature"], columns=["Cell_temperature"])
        voltage = pd.DataFrame(train_data["voltage"], columns=["voltage"])
        current = pd.DataFrame(train_data["current"], columns=["current"])
        Box_temperature = pd.DataFrame(train_data["Box_temperature"], columns=["Box_temperature"])



# if __name__ == '__main__':
#
#     # setting range of data set
#     low_value = 600
#     high_value = 1000
#     step = 100
#
#     # ID
#     ID = 8
#
#     # Giving parameters
#     start_time = '2019-08-31 00:00:00'
#     end_time = '2019-10-14 23:59:00'
#     time_step = "5m"
#
#     # instance
#     homo = Homone(low_value, high_value, step, ID, start_time, end_time, time_step)
#
#     # clean data set
#     clean_data = homo.clean()
#
#     # homo data set
#     train_data = homo.homoset(clean_data)
#     print(train_data)


