from data import con_influxdb

"""
Given parameters
train data
"""

ID = "'8'"
start_time = '2019-08-30 07:00:00'
end_time = '2019-09-05 17:00:00'
data_type = ""
time_step = "5m"
GWID = "'GWB827EBDF6396'"
site = "'東京'"

if data_type == "raw":
    """
    Voltage_data
    """
    field = "raw_pplc_data"
    col = "voltage"
    voltage = con_influxdb.load_rawIV(field, col, ID, GWID, start_time, end_time, time_step)
    """
    Current_data
    """
    col = "current"
    current = con_influxdb.load_rawIV(field, col, ID, GWID, start_time, end_time, time_step)
else:
    """
    Voltage_data
    """
    field = "normalized_field12"
    col = "voltage"
    voltage = con_influxdb.load_iv(field, col, ID, start_time, end_time, time_step)

    """
    Current_data
    """
    field = "normalized_field12"
    col = "current"
    current = con_influxdb.load_iv(field, col, ID, start_time, end_time, time_step)

    """
    Box_temperature_data
    """
    field = "normalized_field12"
    col = "temperature"
    temperature_box = con_influxdb.load_iv(field, col, ID, start_time, end_time, time_step)


"""
cell_temperature_data
"""
col = "temperature"
temperature_cell = con_influxdb.load_envi(col, start_time, end_time, time_step)

"""
irradiance_data
"""
col = "radiation"
radiation = con_influxdb.load_envi(col, start_time, end_time, time_step)

"""
Satellite Radiation
"""
field = "jma"
col = "radiation"
radiation_satellite = con_influxdb.load_satellite(field, col, site, start_time, end_time, time_step)

"""
Satellite_temperature
"""
field = "jma"
col = "temperature"
temperature_satellite = con_influxdb.load_satellite(field, col, site, start_time, end_time, time_step)