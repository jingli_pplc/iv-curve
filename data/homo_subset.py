"""
Homo sub set based on different range of radiation
using for cross entropy validation
"""

import pandas as pd
import data.data_influx as raw
import numpy as np

# Load main features
radiation_raw = raw.radiation
temperature_raw = raw.temperature_cell
I_raw = raw.current
V_raw = raw.voltage
Box_raw = raw.temperature_box

# Combine all features to a dataset
df = pd.concat([radiation_raw, temperature_raw, I_raw, V_raw, Box_raw], axis=1)
total = df.query("25 < voltage < 32 and current > 0 and Cell_temperature > 0")

# sample set total description
print(total.describe())


"""
Inital subset conditions based on radiation: low, high, step
"""
low_value = 600
high_value = 1000
step = 100
value_list = np.arange(low_value, high_value, step)
str_rad = [str(a) for a in value_list]
list_len = len(str_rad)
# df = {}

# Initial list for saving sub-data
subls = []
# Initial number for finding the smallest size of subset
number_sub = 1000
# Initial empty dataframe
train_data = pd.DataFrame()

"""
Subset for training based on radiation
Finding the smallest subset size
"""
for i in np.arange(0, list_len):
    if value_list[i] < (high_value-step):
        df = total.query(str_rad[i] + " <= radiation < " + str_rad[i + 1])
    elif value_list[i] >= (high_value-step):
        df = total.query("radiation >= " + str_rad[i])
    subls.append(df)
    data_number = df["radiation"].count()
    if data_number < number_sub:
        number_sub = data_number

print(number_sub)
print(subls)
# """
# Making a homogenious subset based on the smallest subset size
# """
# for i in np.arange(0, list_len):
#     subls[i] = subls[i].sample(n=number_sub)
#     train_data = train_data.append(subls[i])
#
# """
# Setting the data frame format as same as the raw data
# """
# radiation = pd.DataFrame(train_data["radiation"], columns=["radiation"])
# temperature_cell = pd.DataFrame(train_data["Cell_temperature"], columns=["Cell_temperature"])
# voltage = pd.DataFrame(train_data["voltage"], columns=["voltage"])
# current = pd.DataFrame(train_data["current"], columns=["current"])
# Box_temperature = pd.DataFrame(train_data["Box_temperature"], columns=["Box_temperature"])
#
# print(radiation.count())

