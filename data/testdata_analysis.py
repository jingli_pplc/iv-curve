"""
This is for the visualization of the data-set distribution
"""
import seaborn as sns
import matplotlib.pyplot as plt
import data.test_dataset as test

"""                                                                                                   
Measured data from sensor                                                                             
"""
irradiance = test.radiation  # unit w/m2

# # irradiance.plot()
# # plt.show()
cell_temperature = test.temperature_cell + 273.15  # measure data from database cell_temperature[id][t]
# # cell_temperature.plot()
# # plt.show()
module_voltage = test.voltage
# # module_voltage.plot()
#
module_current = test.current
# module_current.plot()
# plt.show()


# sns.distplot(x,color="#81ececff")
sns.distplot(irradiance, kde=False, label="Radiation distribution",
                     hist_kws = {'range': (0, 1200)},
                    )
plt.show()
sns.distplot(cell_temperature, label="Cell temperature", kde=False, hist_kws={'range': (270, 360)})
plt.show()
sns.distplot(module_voltage, label="Voltage", kde=False, hist_kws={'range': (10, 50)})
plt.show()
sns.distplot(module_current, label="Current", kde=False)
plt.show()