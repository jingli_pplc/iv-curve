"""
Data from influx database
"""
from data import con_influxdb

class pplc:

    def __init__(self, ID, start_time, end_time, data_type, time_step, GWID, site):
        self.ID = ID
        self.start = start_time
        self.end = end_time
        self.data_type = data_type  # raw type: no filter
        self.time_step = time_step
        self.GWID = GWID
        self.site = site

    """
    Loading ground measurement data
    """
    def load_ground(self):
        if self.data_type == "raw":
            """
            Voltage_data
            """
            field = "raw_pplc_data"
            col = "voltage"
            voltage = con_influxdb.load_rawIV(field, col, self.ID, self.GWID, self.start, self.end, self.time_step)
            """
            Current_data
            """
            col = "current"
            current = con_influxdb.load_rawIV(field, col, self.ID, self.GWID, self.start, self.end, self.time_step)
        else:
            """
            Voltage_data
            """
            field = "normalized_field12"
            col = "voltage"
            voltage = con_influxdb.load_iv(field, col, self.ID, self.start, self.end, self.time_step)

            """
            Current_data
            """
            field = "normalized_field12"
            col = "current"
            current = con_influxdb.load_iv(field, col, self.ID, self.start, self.end, self.time_step)

            """
            Box_temperature_data
            """
            field = "normalized_field12"
            col = "temperature"
            temperature_box = con_influxdb.load_iv(field, col, self.ID, self.start, self.end, self.time_step)

        """
        cell_temperature_data
        """
        col = "temperature"
        temperature_cell = con_influxdb.load_envi(col, self.start, self.end, self.time_step)

        """
        irradiance_data
        """
        col = "radiation"
        radiation = con_influxdb.load_envi(col, self.start, self.end, self.time_step)

        return {"voltage": voltage, "current": current, "radiation": radiation,
                "temperature_cell": temperature_cell, "temperature_box": temperature_box}


    def load_satellite(self):
        """
        Satellite Radiation
        """
        field = "jma"
        col = "radiation"
        radiation_satellite =\
            con_influxdb.load_satellite(field, col, self.site, self.start, self.end, self.time_step)

        """
        Satellite_temperature
        """
        field = "jma"
        col = "temperature"
        temperature_satellite = \
            con_influxdb.load_satellite(field, col, self.site, self.start, self.end, self.time_step)

        return {"radiation": radiation_satellite, "temperature": temperature_satellite}


if __name__ == '__main__':
    """
    Given parameters
    train data
    """
    Number = 8
    ID = "'" + str(Number) + "'"
    start_time = '2019-08-31 00:00:00'
    end_time = '2019-10-14 23:59:00'
    data_type = ""
    time_step = "5m"
    GWID = "'GWB827EBDF6396'"
    site = "'東京'"

    ins = pplc(ID, start_time, end_time, data_type, time_step, GWID, site)
    print(ins.GWID)
    df = ins.load_ground()
    print(df.get("temperature_cell"))
