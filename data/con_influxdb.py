import influxdb
import pytz

"""
Connect to database: influxdb
measurements: 'field12', 'field12-environmentsensor', 'field12-powerconditioner',
              field12_normalize', 'measurement_data', 'normalized_field12',
              'normalized_kanden', 'normalized_okanokoen' et al
            
"""

client = influxdb.DataFrameClient (host='analytics.pplc.co', port=18086, username='pplc-readonly',
                         password='kB6QjfnG', ssl= True, database="pplc")

"""
Query function:

           load_iv 
               is the function loading data from normalized_field12 or other tables

           load_envi 
               is the specific function only loading data from field12-environmentsensor.
               The data from field12-environmentsensor are still in nano second scale and
               are required to change the format to second

"""
# df = client.query(("select * from \"field12-environmentsensor\""))
# print(df['field12-environmentsensor'].loc["2019-09-01 23:00:00+00:00"])

tz = pytz.timezone("Asia/Tokyo")


def load_iv(field,col, ID, start_time, end_time, time_step):
    if (field == "normalized_field12") and (col == "temperature"):
        rename = "Box_temperature"
    # elif (field == "field12-environmentsensor") and (col == "temperature"):
    #     rename = "Cell_temperature"
    else:
        rename = col

    box_temp = client.query("SELECT mean(" + col +") as " + rename +
                            " FROM " + field +
                            " where ID= " + ID +
                            # "AND time >= " + start_time +
                            # "AND time <= " + end_time +
                            "GROUP BY time(" + time_step + ")"
                            # "fill(none)"
                            )
    box_temp = box_temp[field].fillna(0)
    df = box_temp
    df.index = df.index.tz_convert(tz)
    df.index = df.index.strftime('%Y-%m-%d %H:%M:%S')
    df = box_temp.loc[start_time:end_time]
    return df


def load_envi(col, start_time, end_time, time_step):
    if col == "temperature":
        rename = "Cell_temperature"
    else:
        rename = col

    data = client.query("SELECT mean(" + col +") as " + rename +
                        " FROM \"field12-environmentsensor\" "
                        "GROUP BY time(" + time_step + ")"
                        "fill(null)"
                        )

    temp = data['field12-environmentsensor'].fillna(0)
    temp.index = temp.index.tz_convert(tz)
    temp.index = temp.index.strftime('%Y-%m-%d %H:%M:%S')
    # print(temp)
    df = temp.loc[start_time:end_time]
    # print(df)
    return df


def load_rawIV(field, col, ID, GWID, start_time, end_time, time_step):
    rename = col
    box_temp = client.query("SELECT mean(" + col + ") as " + rename +
                            " FROM " + field +
                            " where ID= " + ID +
                            "AND GWID = " + GWID +
                            # "AND signal_count >= '2' "
                            # "AND time <= " + end_time +
                            "GROUP BY time(" + time_step + ")"
                            "fill(null)"
                            )
    box_temp = box_temp[field].fillna(0)
    df = box_temp
    df.index = df.index.tz_convert(tz)
    df.index = df.index.strftime('%Y-%m-%d %H:%M:%S')
    df = box_temp.loc[start_time:end_time]
    return df


def load_satellite(field, col, site, start_time, end_time, time_step):
    if (field == "normalized_field12") and (col == "temperature"):
        rename = "Box_temperature"
    # elif (field == "field12-environmentsensor") and (col == "temperature"):
    #     rename = "Cell_temperature"
    else:
        rename = col

    temp = client.query("SELECT mean(" + col +") as " + rename +
                            " FROM " + field +
                            " where site= " + site +
                            # "AND time >= " + start_time +
                            # "AND time <= " + end_time +
                            " GROUP BY time(" + time_step + ")"
                            # "fill(none)"
                            )
    temp = temp[field].fillna(0)
    df = temp
    df.index = df.index.tz_convert(tz)
    df.index = df.index.strftime('%Y-%m-%d %H:%M:%S')
    df = temp.loc[start_time:end_time]
    return df








