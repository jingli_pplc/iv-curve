"""
This is for the visualization of the data-set distribution
"""
import seaborn as sns
import matplotlib.pyplot as plt
import data.combine_data as flux

"""                                                                                                   
Measured data from sensor                                                                             
"""
irradiance = flux.radiation  # unit w/m2

# # irradiance.plot()
# # plt.show()
cell_temperature = flux.temperature_cell + 273.15  # measure data from database cell_temperature[id][t]
# # cell_temperature.plot()
# # plt.show()
module_voltage = flux.voltage
# # module_voltage.plot()
#
module_current = flux.current
# module_current.plot()
# plt.show()


# sns.distplot(x,color="#81ececff")
sns.distplot(irradiance, kde=True, axlabel="Radiation (w/m2)",
                     hist_kws = {'range': (0, 1200)},
                    )
plt.show()
sns.distplot(cell_temperature, kde=True,
             axlabel="Cell Temperature (K)", hist_kws={'range': (290, 360)})
plt.show()
sns.distplot(module_voltage, kde=False, axlabel="Voltage (V)", hist_kws={'range': (0, 40)})
plt.show()
sns.distplot(module_current, kde=True, axlabel="Current (A)")
plt.show()


