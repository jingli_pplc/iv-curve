"""
This is used to comparing the different time range of datasets
with statistical method
"""
from data import con_influxdb
from scipy import stats
ID = "'8'"
time_step = "5m"

start_a = '2019-08-30 07:00:00'
end_a = '2019-08-30 17:00:00'

start_b = '2019-09-08 07:00:00'
end_b = '2019-09-08 17:00:00'

"""
irradiance_data
"""
col = "radiation"
radiation_a = con_influxdb.load_envi(col, start_a, end_a, time_step)
radiation_b = con_influxdb.load_envi(col, start_b, end_b, time_step)

"""
The calculated t-statistic

P-value: 
    if close to 1: than same
    if close to 0: than different
"""
tp = stats.ttest_ind(radiation_a, radiation_b)

print(tp)