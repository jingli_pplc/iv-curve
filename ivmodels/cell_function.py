import numpy as np
"""
notes
cell is a physical model to simulate the pv cell' character

Reference <paper>


by jing 28th, Auguest

"""

"""import libraries"""

"""
Parameters
----------
Iph : float
  Photocurrent, proportional to the irradiance E
E : float
  Irradiance
const : float or function
  Const is the efficiency of cell under the irradiance
"""


def photo_current(const, E):
    Iph = const*E
    return Iph


def photo_I(E, cell_temperature,const_a, const_b):
    Iph = (const_a + const_b*cell_temperature)*E
    return Iph
"""
cell temperature[t] -> ni[t]
average of free electrons
"""


def free_electron(N_material, wg, cell_temperature, thermo_k):
    ni = N_material * np.e ** (-wg /(2 * thermo_k * cell_temperature))
    return ni


"""
ni[t] -> Is[t]
based on material model
"""


def satu_current_material(ni, q, A, Dn, Dp, Ln, Lp, Na, Nd):
    Is = A * (q * Dn * ni ** 2 / (Ln * Na) + q * Dp * ni ** 2 / (Lp * Nd))
    return Is


"""
ni[t] -> Is[t]
based on math model
"""


def satu_current_simple(ni, const_Is):
    Is = ni**2 * const_Is
    return Is


#Todo applied cell current function (Iph, Is, m, V, VT)
def current(Iph, Is, m, V, VT):
    V_temp = V/(m*VT)
    ID = Is*(np.e**V_temp-1)
    I = Iph - ID
    # current = I.fillna(0)
    current = I
    return current


"""
VT : float 
  thermal voltage
"""


def thermal_voltage(thermo_k, temperature, q):
    VT = thermo_k*temperature/q
    return VT


def Voc(m, VT, Iph, Is):
    temp = np.log(Iph/Is + 1)
    Voc = m * VT * temp
    return Voc
