"""
This class is based on the function of
"""
from ivmodels import cell_function
from ivmodels import constant


def Isc(irradiance, cell_temperature, const_a, const_b):
    """
    Irradiance related:
    function = (a+bTc)E
    """
    Iph = cell_function.photo_I(irradiance, cell_temperature, const_a, const_b)  # Iph[t]
    return Iph


def IV_irr(irradiance, cell_temperature, cell_voltage, const_a, const_b, const_Is, m):
    """
    Irradiance related
    """

    Iph = cell_function.photo_I(irradiance, cell_temperature, const_a, const_b) # Iph[t]

    """
    material related 
    """
    N_material = constant.N_material
    bandgap = constant.bandgap()

    """
    cell temperature related
    """

    free_electrons = cell_function.free_electron(N_material, bandgap, cell_temperature, constant.thermo_k)  # unit: cm-3

    # cell_T -> Is[t] tuning data or physical model from satu_current
    Is = cell_function.satu_current_simple(free_electrons, const_Is)
    thermo_voltage = cell_function.thermal_voltage(constant.thermo_k, cell_temperature, constant.q)

    """
    IV curve
    """

    cell_current = cell_function.current(Iph, Is, m, cell_voltage, thermo_voltage)
    # print(cell_current)
    return cell_current


def Voc(irradiance, cell_temperature, const_a, const_b, const_Is, m):
    """
    Irradiance related
    """

    Iph = cell_function.photo_I(irradiance, cell_temperature, const_a, const_b) # Iph[t]

    """
    material related 
    """
    N_material = constant.N_material
    bandgap = constant.bandgap()

    """
    cell temperature related
    """

    free_electrons = cell_function.free_electron(N_material, bandgap, cell_temperature, constant.thermo_k)  # unit: cm-3

    # cell_T -> Is[t] tuning data or physical model from satu_current
    Is = cell_function.satu_current_simple(free_electrons, const_Is)
    thermo_voltage = cell_function.thermal_voltage(constant.thermo_k, cell_temperature, constant.q)

    """
    IV curve
    """
    Voc = cell_function.Voc(m, thermo_voltage, Iph, Is)

    return Voc