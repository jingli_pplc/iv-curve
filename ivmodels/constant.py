"""
constant definition
boltzman_const : float
  boltzman constant

N_material : float
  effective density of states

Dn : int
  Number of XXX electrons

m : float
  related to material's structure

Dn : float
   diffusion constant of the electron

"""
# absolute constant
boltzman_const: float = 1.38E-23  # unit : J/K
thermo_k : float = 8.62E-5  #unit eV/K
# TODO figure out the value of q and e
q = 1  # unit = ev

# constant based on material (might be requred to tune)
#Todo built a datatable of N_material
N_material: float = 3E13  # unit: m3  3E19 cm3
Na: int = 10E15 # unit: cm3
Nd: int = 10E17 # unit: cm3
Dp: float = 0.5
# 35 cm2/s for c-Si
Dn: float = 35

# Si 50~500 um
Ln: float = 100E-6 # unit : cm
Lp: float = 1
# #Todo: can be tuned
# m : float = 1.22

"""
wg : float
  Bandgap Wg is depending on the material' character
  nomally silicon is 1.12 ev
"""
#Todo built a datatable of bandgap for different material
#Todo check the bandgap (irradiance, temperature) function
#Todo if bandgap is not constant, add a if/else cholse


def bandgap():
    wg: float = 1.12
    return wg


"""
Hyper parameters tuning self define range
"""


def drange(x, y, jump):
    list = []
    while x < y:
        yield x
        x += jump
        list.append(x)
    return list

