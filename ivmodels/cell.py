from ivmodels import cell_function
from ivmodels import constant


def IV(irradiance, cell_temperature, cell_voltage, const_irr, const_Is, m):
    """
    Irradiance related
    """

    Iph = cell_function.photo_current(const_irr, irradiance)  # Iph[t]

    """
    material related 
    """
    N_material = constant.N_material
    bandgap = constant.bandgap()

    """
    cell temperature related
    """

    free_electrons = cell_function.free_electron(N_material, bandgap, cell_temperature, constant.thermo_k)  # unit: cm-3

    # cell_T -> Is[t] tuning data or physical model from satu_current
    Is = cell_function.satu_current_simple(free_electrons, const_Is)
    thermo_voltage = cell_function.thermal_voltage(constant.thermo_k, cell_temperature, constant.q)

    """
    IV curve
    """
    Voc = cell_function.Voc(m, thermo_voltage, Iph, Is)

    cell_current = cell_function.current(Iph, Is, m, cell_voltage, thermo_voltage)
    # print(cell_current)
    return cell_current


def Voc(irradiance, cell_temperature, const_irr, const_Is, m):
    """
    Irradiance related
    """

    Iph = cell_function.photo_current(const_irr, irradiance)  # Iph[t]

    """
    material related 
    """
    N_material = constant.N_material
    bandgap = constant.bandgap()

    """
    cell temperature related
    """

    free_electrons = cell_function.free_electron(N_material, bandgap, cell_temperature, constant.thermo_k)  # unit: cm-3

    # cell_T -> Is[t] tuning data or physical model from satu_current
    Is = cell_function.satu_current_simple(free_electrons, const_Is)
    thermo_voltage = cell_function.thermal_voltage(constant.thermo_k, cell_temperature, constant.q)

    """
    IV curve
    """
    Voc = cell_function.Voc(m, thermo_voltage, Iph, Is)

    return Voc


def Isc(irradiance, const_irr):
    """
    Irradiance related
    """

    Iph = cell_function.photo_current(const_irr, irradiance)  # Iph[t]

    return Iph