"""
This script is using to connect Kouda'nanotube
to real time visualize the graph in influxdb

"""
import json

class Nanotube:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def datago(self):
        data = {}
        data['columns'] = []
        data['columns'].append(
            {"text": "A.x", "type": "number"}
        )
        data['columns'].append(
            {"text": "A.y", "type": "number"}
        )
        data['columns'].append(
            {"text": "A.z", "type": "number"}
        )
        data['columns'].append(
            {"text": "A.m", "type": "number"}
        )

        data['rows'] = []
        data['rows'].append(
                [self.x, self.y, 0, 0]
            )


        data['type'] = 'table'

        with open('/home/sita/Documents/nano/CarbonNanotubeClient_0.0.1/CarbonNanotubeClient/query', 'w') as outfile:
            json.dump([data], outfile)





# if __name__== "__main__":
#     ins = Nanotube(2, 4)
#     ins.datago()


