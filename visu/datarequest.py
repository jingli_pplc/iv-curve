import json

data = {}
data['columns'] = []
data['columns'].append(
    {"text": "A.x", "type": "number"}
)
data['columns'].append(
    {"text": "A.y", "type": "number"}
)
data['columns'].append(
    {"text": "A.z", "type": "number"}
)
data['columns'].append(
    {"text": "A.m", "type": "number"}
)
data['columns'].append(
    {"text": "A.s", "type": "number"}
)
data['rows'] = []
data['rows'].append(
    [0, 30000, 2, 45, 67]
)
data['rows'].append(
    [6, 10000, 2, 89, 78]
)
data['rows'].append(
    [78, 80000, 2, 909, 99999])
data['rows'].append(
    [767, 20000, 67, 90, 78]
)

data['type'] = 'table'


print("Content-Type: text/plain")
print("")

string = json.dumps([data])
# print(string)