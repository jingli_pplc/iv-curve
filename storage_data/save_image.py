import io
from PIL import Image
import matplotlib.pyplot as plt
import base64
import storage_data.connect_mysql as connect


def convertToBinaryData(filename):
    # Convert digital data to binary format
    with open(filename, 'rb') as file:
        binaryData = file.read()
    return binaryData


def convertTobidata(filename):
    with open(filename, "rb") as imageFile:
        str = base64.b64encode(imageFile.read())
        return str


def convertToImage(filename):
    with open(filename, "wb") as imageFile:
        str = base64.b64decode(imageFile.write())
        return str

# plot figure
plt.figure()
plt.plot([1, 2])
plt.title("test")
plt.savefig("buf.jpg")
buf1 = convertTobidata("buf.jpg")
print(buf1)
# insert mysql
query = '''INSERT INTO pvcurve_image (ID,Image) VALUES (%s,%s)'''
a = connect.Datago()
a.write(query, (8, buf1))


# query and plot figure
buf2 = base64.b64decode(buf1)
image_result = open('deer_decode.jpg', 'wb')
image_result.write(buf2)
