import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode


class Datago:
    def write(self, query, val):
        try:
            connection = mysql.connector.connect(host='localhost',
                                                 database='test',
                                                 user='pplc',
                                                 password='pplc_2019Todai')

            mySql_insert_query = query
            val = val
            cursor = connection.cursor()
            result = cursor.execute(mySql_insert_query, val)
            connection.commit()
            print("Record inserted successfully into pvcurve table")
            cursor.close()

        except mysql.connector.Error as error:
            print("Failed to insert record into pvcurve table {}".format(error))

        finally:
            if (connection.is_connected()):
                connection.close()
                print("MySQL connection is closed")

