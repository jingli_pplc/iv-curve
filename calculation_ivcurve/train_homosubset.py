from ivmodels import cell_irr
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import numpy as np
import data.radiation_set as flux
from hyperopt import fmin, hp, tpe, anneal, Trials
from hyperopt.mongoexp import MongoTrials
import data.test_dataset as test

"""
Panel number
"""
"""
Given parameters
"""

max_evals = 3000

"""
Measured data from sensor
"""
irradiance = flux.radiation #unit w/m2

cell_temperature = flux.temperature_cell + 273.15# measure data from database cell_temperature[id][t]

module_voltage = flux.voltage

module_current = flux.current


"""
hyper-parameter space
"""

space = {
    'a': hp.uniform('a', 0.0, 1.0),
    'b': hp.uniform('b', 1E-8, 1E-3),
    "Is": hp.uniform('Is', 1.0E-19, 10.0E-17),
    "m": hp.uniform('m', 1.0, 1.5)
}
"""
Device related
series collection of cells
"""
#Todo move this part to panel.py file
#Todo check the surface area of cell
cell_number = 60
# cell_surface = 0.1524**2
cell_surface = 0.155**2-0.015**2*2
cell_irradiance = cell_surface*irradiance
cell_voltage = (module_voltage / cell_number)# voltage loss duel to cell series collection
cell_current = module_current # current[id][t]

"""
use cell_voltage to predict cell current and compared with measure data
estimator: cell_current, Voc, Iph
Erro measured by RMSE
Array-like input
"""

def objective(parameters):
    a = parameters["a"]
    b = parameters["b"]
    Is = parameters["Is"]
    m = parameters["m"]
    current_estimator = cell_irr.IV_irr(cell_irradiance["radiation"], cell_temperature["Cell_temperature"],
                                        cell_voltage["voltage"],
                                        a, b, Is, m)
    loss = mean_squared_error(cell_current, current_estimator)
    return loss


best = fmin(objective, space, algo=anneal.suggest, max_evals = max_evals)

"""
plot the best parameters results
"""
best_m = best.get('m')
best_a = best.get('a')
best_b = best.get('b')
best_Is = best.get('Is')

best_estimator = cell_irr.IV_irr(cell_irradiance["radiation"], cell_temperature["Cell_temperature"],
                                        cell_voltage["voltage"],
                                        best_a, best_b, best_Is, best_m)

"""
STC condition:

"""
stc_radiation = np.arange(200, 1200, 200)
cell_radiation = stc_radiation * cell_surface
cell_temperature = 25 + 273.15

"""
Plot IV
"""
module_voltage = np.arange(0, 50, 0.01)
cell_voltage = (module_voltage / cell_number)
y = []
Voc_list = []
Isc_list = []

for cell_irradiance in cell_radiation:
    current_estimator = cell_irr.IV_irr(cell_irradiance, cell_temperature, cell_voltage,
                                best_a, best_b, best_Is, best_m)
    Voc = cell_irr.Voc(cell_irradiance, cell_temperature, best_a, best_b, best_Is, best_m)
    Isc = cell_irr.Isc(cell_irradiance, best_a, best_b, cell_temperature)
    Isc_list.append(Isc)
    Voc_list.append(Voc)
    y.append(current_estimator)
print(Isc_list, Voc_list)

plt.scatter(Voc_list, Isc_list)
plt.show()
fig = plt.figure()
x = module_voltage
ax = fig.add_subplot(111)
for i in range(0, len(stc_radiation)):
    ax.scatter(x, y[i], label=str(stc_radiation[i]) + " w/m2")
    ax.set_title("IV_curve under STC condition")
    ax.set_xlabel("Voltage")
    ax.set_ylabel("Current")
    ax.set_xlim((0, 50))
    ax.set_ylim((0, 10))
    ax.legend(loc='upper right')
plt.show()

"""
Plot MPP
"""
stc_radiation = 1000
cell_radiation = stc_radiation * cell_surface
cell_temperature = 25 + 273.15


module_voltage = np.arange(0, 50, 0.01)
cell_voltage = module_voltage / cell_number
cell_voltage = cell_voltage
y = []
P_max = 0
for voltage in cell_voltage:
    current_estimator = cell_irr.IV_irr(cell_radiation, cell_temperature, voltage,
                                best_a, best_b, best_Is, best_m)
    pp = voltage * cell_number * current_estimator
    if pp > P_max:
        P_max = pp
        best_parameters = {'Vpm': voltage*cell_number, 'Ipm': current_estimator}

print(best_parameters)
print("MPP_module: {}".format(P_max))

"""
Efficiency
"""

cell_efficiency = P_max/cell_number/1000/cell_surface
print("cell_efficiency: {}".format(cell_efficiency))

"""
Measured data from sensor
"""
irradiance = test.radiation #unit w/m2
cell_temperature = test.temperature_cell + 273.15
module_voltage = test.voltage
module_current = test.current

"""
Device related
series collection of cells
"""
#Todo move this part to panel.py file
cell_number = 60
cell_surface = 0.1524**2
cell_irradiance = cell_surface*irradiance
cell_voltage = module_voltage / cell_number  # measure data from database voltage[id][t]
cell_voltage = cell_voltage*1
cell_current = module_current # current[id][t]

"""
plot the best parameters results
"""
best_estimator = cell_irr.IV_irr(cell_irradiance["radiation"], cell_temperature["Cell_temperature"],
                                        cell_voltage["voltage"],
                                        best_a, best_b, best_Is, best_m)
erro = mean_squared_error(best_estimator, cell_current)
print("mean squre erro:{}".format(erro))
ax = cell_current.plot()
best_estimator.plot(ax=ax)
plt.xlabel("Current")
ax.legend(["Measured current", "Forecast current", "E_Current"])
plt.show()



