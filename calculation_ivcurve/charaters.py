"""
calculation main characters
such as: Voc, Ish, Power_max, FF

input: radiation, temperature, best parameters
"""
from ivmodels import cell_function, cell_irr
import numpy as np


class IshVoc:

    def __init__(self, radiation, temperature, cell_surface, cell_number, Is, m, a, b):
        self.radiation = radiation
        self.temperature = temperature
        self.cell_surface = cell_surface
        self.cell_number = cell_number
        self.Is = Is
        self.m = m
        self.a = a
        self.b = b

    # using best parameter and radiation, temperature, given voltage to calculate the iv curve

    # calculate Vpm Ipm Power_max Voc
    def vpmipm(self):
        """
        Plot range
        """
        # given voltage range to plot figure
        module_voltage = np.arange(0, 50, 0.001)
        cell_voltage = module_voltage / self.cell_number
        cell_radiation = self.radiation * self.cell_surface
        # ish
        ish = cell_function.photo_I(cell_radiation, self.temperature, self.a, self.b)

        P_max = 0
        voc = 0
        min_current = 0.2
        for voltage in cell_voltage:
            current_estimator = cell_irr.IV_irr(cell_radiation, self.temperature, voltage,
                                        self.a, self.b, self.Is, self.m)
            pp = voltage * self.cell_number * current_estimator
            if pp > P_max:
                P_max = pp
                best_vpmipm = {'Vpm': voltage * self.cell_number, 'Ipm': current_estimator, 'MPP_module': P_max}

            if abs(current_estimator) < min_current:
                min_current = abs(current_estimator)
                voc = voltage*self.cell_number

        # calculate FF
        ff = P_max/(voc*ish)

        best_vpmipm['Voc'] = voc
        best_vpmipm['Ish'] = ish
        best_vpmipm['FF'] = ff
        print(min_current, voc)
        # print("MPP_module: {}".format(P_max))
        return best_vpmipm


# if __name__ == '__main__':
#     # Given STC condition
#     radiation = 1000
#     temperature = 298
#     cell_surface = 0.155 ** 2 - 0.015 ** 2 * 2
#     cell_number = 60
#     a = 0.33045040306332124
#     b = 0.00014010532995675364
#     Is = 5.642875874725294e-17
#     m = 1.1954828621270672
#     ins = IshVoc(radiation, temperature, cell_surface, cell_number, Is, m, a, b)
#     # Ish = ins.ish()
#     vpmipm = ins.vpmipm()
#     Ish = vpmipm.get('Ish')
#     voc = vpmipm.get('Voc')
#     vpm = vpmipm.get('Vpm')
#     ipm = vpmipm.get('Ipm')
#     mpp = vpmipm.get('MPP_module')
#     ff = vpmipm.get('FF')
#     print(Ish, voc, vpm, ipm, mpp, ff)