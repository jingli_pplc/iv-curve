"""
Calculate hyper parameters
"""

from ivmodels import cell_irr
from sklearn.metrics import mean_squared_error
from hyperopt import fmin, hp, anneal, plotting, Trials


class Bestpara:

    def __init__(self, radiation, temperature, voltage, cell_current):
        self.radiation = radiation # cell_irradiance["radiation"]
        self.temperature = temperature# cell_temperature["Cell_temperature"]
        self.voltage = voltage # cell_voltage["voltage"]
        self.current = cell_current

    """
    Objective function : loss is calculated based on the mean squared error
    """

    def objective(self, parameters):
        a = parameters["a"]
        b = parameters["b"]
        Is = parameters["Is"]
        m = parameters["m"]
        current_estimator = cell_irr.IV_irr(self.radiation, self.temperature,
                                            self.voltage,
                                            a, b, Is, m)
        loss = mean_squared_error(self.current, current_estimator)
        return loss

    """
    Search the best parameters results
    """

    def best(self, objective, space, max_evals, do_show=False):
        """
        Optimization hyper parameters
        """
        trials = Trials()
        best = fmin(objective, space, algo=anneal.suggest, trials= trials, max_evals=max_evals)

        if do_show:
            plotting.main_plot_vars(trials, colorize_best=True)
            plotting.main_plot_history(trials)



        print(best)

        # """
        # Get the best parameters results
        # """
        # best_m = best.get('m')
        # best_a = best.get('a')
        # best_b = best.get('b')
        # best_Is = best.get('Is')
        # print(best_a, best_b, best_Is, best_m)
        return best

